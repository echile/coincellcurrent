import subprocess
import pathlib
import os
from shutil import copyfile

class DecaTest():
    def __init__(self, path, device=0, speed=1):
        self._path = path
        self._device = device
        self._speed = speed
        self._exe_path = pathlib.Path(self._path)
        self._command_path = pathlib.Path(self._exe_path.parent.parent, 'command_files')
        #os.chdir(str(self._exe_path.parent))


    @property
    def device(self):
        return self._device

    @device.setter
    def set_device(self, device):
        self._device = device

    def _run(self, cmd):
        prog = f'{self._path} @{self._device} {cmd}'
        proc = subprocess.run(prog, capture_output=True, cwd=self._exe_path.parent)
        return proc.stdout.decode()

    def reset(self):
        outp = self._run('-x')
        if "not 'ready'" in outp:
            raise RuntimeError('Device not reset correctly')
        return outp

    def status(self):
        return self._run('-y')

    def help(self):
        return self._run('-h')

    def script(self, script):
        return self._run(f'-i {script}')

    def list_scripts(self):
        tmp = [p.name for p in self._command_path.glob('*.txt')]
        return '\n'.join(tmp)

    def get_script_contents(self, script:str):
        return pathlib.Path(self._command_path, script).read_text()


def main():
    dev = DecaTest(r'C:\Decatest\Unpprovisioned_Parts\decatestng-master_b0\decatestng-master_b0\Release\decaTestNG.exe',
        1, 1)

    print(dev.help())
    print(dev.status())
    print(dev.list_scripts())
    dev.reset()

    print(dev.script('HTOL_B0_Full_v3p2_SIP.txt'))

    
    
    # prog =  -y'
    # proc = subprocess.run(prog, capture_output=True)
    # print(int(proc.stdout.decode().split('RX Frames OK    : ')[1].split()[0], 16))

if __name__ == '__main__':
    main()