import argparse
from Instruments import keysightmso, keithley
from Decatest import decasetup
import matplotlib.pyplot as plt
import struct
import time
from datetime import datetime
import pandas as pd
import traceback
from pathlib import Path
from saleae import automation

APP_FUNCS = ['save_scope_setup', 
    'capture_data']

def run_modes(scope:keysightmso.KeysightMSO, dev:decasetup.DecaTest, 
    scope_setup:str, command_file:str):
    dev.reset()
    scope.system_setup_recall(scope_setup)
    scope.stop()
    scope.opc_query()
    scope.single()
    time.sleep(.1)
    #scope.run()
    #scope.trigger_force()
    print(dev.script(command_file))

def setup_ps(ps:keithley.Keithley2230):
    voltages = [3.3, 0, 1.8]
    currents = [.5, 0.02, 0.02]

    ps.reset()
    time.sleep(1)

    for i, (volt, curr) in enumerate(zip(voltages, currents), start=1):
        ps.set_voltage_limit(True, volt * 1.1, channel=i)
        ps.set_voltage(volt, channel=i)
        ps.set_current_limit(curr, channel=i)
        ps.set_output(True, channel=i)

    time.sleep(1)

def save_data(x_axis:list, traces:list, res_dir:Path) -> None:
    df = pd.DataFrame(traces, index=x_axis)
    df.to_csv(res_dir.joinpath('traces.csv'))

def parse_args():
    parser = argparse.ArgumentParser(description='Current Meas App')

    parser.add_argument('-f', '-func', dest='func', type=str, choices=APP_FUNCS,
        required=True)
    parser.add_argument('-s', '-scope_setup', dest='scope_setup', type=str,
        required=True)
    parser.add_argument('-c', '-command_file', dest='command_file', type=str,
        default='')

    return parser.parse_args()

def saleae_config(id:str):
    manager = automation.Manager.connect(port=10430)
    dev_config = automation.LogicDeviceConfiguration(
        enabled_analog_channels=[0, 1, 2, 3, 4],
        enabled_digital_channels=[0, 1, 2, 3],
        analog_sample_rate=12_500_000,
        digital_sample_rate=100_000_000,
        digital_threshold_volts=1.8,
    )
    
    cap_config = automation.CaptureConfiguration(capture_mode=
        automation.DigitalTriggerCaptureMode(
        trigger_type=automation.DigitalTriggerType.FALLING,
        trigger_channel_index=3, 
        trim_data_seconds = .5,
        after_trigger_seconds=1))

    capture = manager.start_capture(device_id='304D97F7B74D952D',
        device_configuration=dev_config,
        capture_configuration=cap_config)

    return manager, capture

def saleae_capture(manager, capture:automation.Capture, res_dir:Path):
    capture.wait()
    spi_analyzer = capture.add_analyzer('SPI', label='Test Analyzer', 
        settings={'MOSI': 0,
            'MISO': 1,
            'Clock': 2,
            'Enable': 3,
            'Bits per Transfer': '8 Bits per Transfer (Standard)'})
    capture.export_raw_data_csv(directory=str(res_dir.absolute()), digital_channels=
        [0, 1, 2, 3], analog_channels=[0,1,2,3,4])
    capture.export_data_table(filepath=str(res_dir.joinpath('spi_export.csv').absolute()),
        analyzers=[spi_analyzer])
    capture.save_capture(filepath=str(res_dir.joinpath('capture.sal').absolute()))
    
def close_saleae(manager:automation.Manager, capture:automation.Capture):
    capture.close()
    manager.close()        


def main(func:str, scope_setup:str, command_file:str):
    try:        
        
        scope = keysightmso.KeysightMSO('TCPIP0::a-mx6004a-70851::INSTR',timeout=10000)
        scope._rsrc.clear()
        
        if func == APP_FUNCS[0]:
            scope.system_setup_save(scope_setup)
        elif func == APP_FUNCS[1]:
            res_dir = Path('Results', f'{command_file.split(".")[0]}_{datetime.now().strftime("%Y_%m_%d-%H_%M_%S")}')
            res_dir.mkdir()
            ps = keithley.Keithley2230('USB0::0x05E6::0x2230::9031749::INSTR')
            dev = decasetup.DecaTest(r'C:\Decatest\Unpprovisioned_Parts\decatestng-master_b0\decatestng-master_b0\Release\decaTestNG.exe',
            device=1, speed=1)
            ps.reset()
            setup_ps(ps)


            manager, capture = saleae_config('304D97F7B74D952D')    
            run_modes(scope, dev, scope_setup, command_file)

            time.sleep(2)
            saleae_capture(manager, capture, res_dir)

            acq_pts = int(scope.acquisition_points())

            scope.opc_query()
            res_dir.joinpath('scope_screen.png').write_bytes(
                scope.display_data())
            scope.waveform_format(keysightmso.WaveformFormat.byte)
            
            x_inc = scope.waveform_x_increment()
            x_org = scope.waveform_x_origin()
            x_axis = [(x * x_inc) + x_org for x in range(acq_pts)]

            traces = {}
            for i in range(4):
                scope.waveform_source(source=keysightmso.WaveformSource.channel, index=i+1)
                scope.waveform_points_mode(keysightmso.WaveformPointsMode.raw)
                y_inc = scope.waveform_y_increment()
                y_org = scope.waveform_y_origin()
                y_ref = scope.waveform_y_reference()
                tmp = scope.waveform_data()
                traces[f'byte_ch{i+1}'] = tmp
                traces[f'real_ch{i+1}'] = [((y - y_ref) * y_inc) + y_org for y in tmp]
                #plt.plot(x_axis, traces[f'ch{i+1}'], label=f'ch{i+1}')

            res_dir.joinpath(command_file).write_text(
                dev.get_script_contents(script=command_file))
                
            save_data(x_axis=x_axis, traces=traces, res_dir=res_dir)

            for key, trace in traces.items():
                if 'byte' in key:
                    plt.plot(x_axis, trace, label=key)
            plt.legend()
            plt.savefig(res_dir.joinpath('byte_test.png'))
            plt.clf()
            for key, trace in traces.items():
                if 'real' in key:
                    plt.plot(x_axis, trace, label=key)
            plt.legend()
            plt.savefig(res_dir.joinpath('real_test.png'))
        #plt.show()

        print('place holder')
    except Exception:
        traceback.print_exc()
    finally:
        close_saleae(manager, capture)
            

if __name__ == '__main__':
    args = parse_args()
    main(args.func, args.scope_setup, args.command_file)