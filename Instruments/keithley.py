from Instruments.baseinstrument import BaseInstrument

class Keithley2230(BaseInstrument):
    def _set_channel(self, channel:int=1) -> None:
        self._rsrc.write(f'INST:NSEL {channel}')

    def set_voltage_limit(self, enabled:bool, voltage:float, channel:int=1) -> None:
        self._set_channel(channel)
        self._rsrc.write(f'SOUR:VOLT:LIM:STAT {1 if enabled else 0}')
        self._rsrc.write(f'SOUR:VOLT:LIM {voltage}')

    def set_voltage(self, voltage:float, channel:int=1) -> None:
        self._set_channel(channel)
        self._rsrc.write(f'SOUR:VOLT {voltage}')

    def set_current_limit(self, current:float, channel=1) -> None:
        self._set_channel(channel)
        self._rsrc.write(f'SOUR:CURR {current}')

    def set_output(self, enabled:bool, channel:int=1) -> None:
        self._set_channel(channel)
        self._rsrc.write(f'SOUR:CHAN:OUTP {1 if enabled else 0}')

    def meas_voltage(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'MEAS:VOLT? CH{channel}'))

    def meas_current(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'MEAS:CURR? CH{channel}'))

    def meas_power(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'MEAS:POW? CH{channel}'))

    def fetch_voltage(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'FETC:VOLT? CH{channel}'))

    def fetch_current(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'FETC:CURR? CH{channel}'))

    def fetch_power(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'FETC:POW? CH{channel}'))

    