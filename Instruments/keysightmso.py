from Instruments.baseinstrument import BaseInstrument
from typing import List
from enum import Enum

class AcquitionType(str, Enum):
    normal = 'NORM'
    average = 'AVER'
    high_resolution = 'HRES'
    peak = 'PEAK'

class WaveformSource(str, Enum):
    channel = 'CHAN'
    pod = 'POD'
    bus = 'bus'
    func = 'FUNC'
    math = 'MATH'
    memory = 'WMEM'
    serial = 'SBUS'

class ChannelUnits(str, Enum):
    volt = 'VOLT'
    ampere = 'AMP'

class ChannelImpedance(str, Enum):
    onemeg = 'ONEM'
    fifty = 'FIFT'

class ChannelCoupling(str, Enum):
    ac = 'AC'
    dc = 'DC'

class TriggerSweepMode(str, Enum):
    auto = 'AUTO'
    normal = 'NORM'

class TriggerType(str, Enum):
    edge = 'EDGE'
    glitch = 'GLIT'
    pattern = 'PATT'
    tv = 'TV'
    delay = 'DEL'
    edge_burst = 'EBUR'
    or_oper = 'OR'
    runt = 'RUNT'
    setup_hold = 'SHOL'
    transition = 'TRAN'
    serial = 'SBUS'

class TriggerEdgeCoupling(str, Enum):
    ac = 'AC'
    dc = 'DC'
    lf_reject = 'LFR'

class TriggerEdgeLevelSource(str, Enum):
    channel = 'CHAN'
    digital = 'DIG'
    external = 'EXT'

class TriggerEdgeSource(str, Enum):
    channel = 'CHAN'
    digital = 'DIG'
    external = 'EXT'
    line = 'LINE'
    wave_gen = 'WGEN'
    wave_mod = 'WMOD'

class TriggerEdgeSlope(str, Enum):
    negative = 'NEG'
    positive = 'POS'
    either = 'EITH'
    alternate = 'ALT'

class WaveformFormat(str, Enum):
    word = 'WORD'
    byte = 'BYTE'
    ascii = 'ASC'

class WaveformPointsMode(str, Enum):
    normal = 'NORM'
    maximum = 'MAX'
    raw = 'RAW'

        
class KeysightMSO(BaseInstrument):
    def set_vertical_scale(self, scale:float, channel:int=1):
        self._rsrc.write(f'CHAN{channel}:SCAL {scale}')
        
    def set_vertical_offset(self, offset:float, channel:int=1):
        self._rsrc.write(f'CHAN{channel}:OFFS {offset}')

    def get_vertical_scale(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'CHAN{channel}:SCAL?'))
        
    def get_vertical_offset(self, channel:int=1):
        return float(self._rsrc.write(f'CHAN{channel}:OFFS?'))

    def set_horizontal_scale(self, scale:float):
        self._rsrc.write(f'TIM:SCAL {scale}')
        
    def set_horizontal_offset(self, offset:float):
        self._rsrc.write(f'TIM:POS {offset}')

    def get_horizontal_scale(self, channel:int=1) -> float:
        return float(self._rsrc.query(f'CHAN{channel}:SCAL?').strip())
        
    def get_horizontal_offset(self, channel:int=1):
        return float(self._rsrc.write(f'CHAN{channel}:OFFS?').strip())
    
    def acquisition_points(self):
        return int(self._rsrc.query('ACQ:POIN?').strip())

    def run(self):
        self._rsrc.write(f'RUN')
    
    def stop(self):
        self._rsrc.write('STOP')

    def single(self):
        self._rsrc.write(f'SING')

    def set_channel_scale(self, channel:int, scale:float) -> None:
        self._rsrc.write(f'CHAN{channel}:SCAL {scale}')

    def set_channel_offset(self, channel:int, offset:float) -> None:
        self._rsrc.write(f'CHAN{channel}:OFFS {offset}')

    def set_channel_impedance(self, channel:int, impedance:ChannelImpedance) -> None:
        self._rsrc.write(f'CHAN{channel}:IMP {impedance}')

    def set_channel_units(self, channel:int, units:ChannelUnits) -> None:
        self._rsrc.write(f'CHAN{channel}:IMP {units}')

    def set_channel_coupling(self, channel:int, coupling:ChannelCoupling) -> None:
        self._rsrc.write(f'CHAN{channel}:COUP {coupling}')

    def set_channel_probe_zoom(self, channel:int, enabled:bool) -> None:
        self._rsrc.write(f'CHAN{channel}:PROB:ZOOM {1 if enabled else 0}')

    def set_trigger_sweep_mode(self, mode:TriggerSweepMode) -> None: 
        self._rsrc.write(f'TRIG:SWE {mode}')

    def set_trigger_type(self, type:TriggerType) -> None:
        self._rsrc.write(f'TRIG:MODE {type}')

    def trigger_force(self) -> None:
        self._rsrc.write(f'TRIG:FORC')

    def set_trigger_edge_coupling(self, coupling:TriggerEdgeCoupling) -> None:
        self._rsrc.write(f'TRIG:EDGE:COUP {coupling}')

    def set_trigger_edge_level(self, level:float, source:TriggerEdgeLevelSource=TriggerEdgeLevelSource.channel, index:int=1):
        src = source if source == TriggerEdgeLevelSource.external else f'{source}{index}'
        self._rsrc.write(f'TRIG:EDGE:LEV {level},{src}') 

    def set_trigger_edge_source(self, level:float, source:TriggerEdgeSource=TriggerEdgeSource.channel, index:int=1):
        sources = [TriggerEdgeSource.channel, TriggerEdgeSource.digital, TriggerEdgeSource.wave_gen]
        src = f'{source}{index}' if source in sources else {source}
        self._rsrc.write(f'TRIG:EDGE:LEV {level},{src}') 

    def set_trigger_edge_slope(self, slope:TriggerEdgeSlope) -> None:
        self._rsrc.write(f'TRIG:EDGE:SLOP {slope}')

    def waveform_x_increment(self) -> float:
        return float(self._rsrc.query('WAV:XINC?').strip())

    def waveform_x_origin(self) -> float:
        return float(self._rsrc.query('WAV:XOR?').strip())

    def waveform_x_reference(self) -> int:
        return float(self._rsrc.query('WAV:XREF?').strip())

    def waveform_y_increment(self) -> float:
        return float(self._rsrc.query('WAV:YINC?').strip())

    def waveform_y_origin(self) -> float:
        return float(self._rsrc.query('WAV:YOR?').strip())

    def waveform_y_reference(self) -> int:
        return float(self._rsrc.query('WAV:YREF?').strip())

    def waveform_format(self, format:WaveformFormat) -> None:
        self._rsrc.write(f'WAV:FORM {format}')

    def waveform_points_mode(self, mode:WaveformPointsMode) -> None:
        self._rsrc.write(f'WAV:POIN:MODE {mode}')

    def waveform_data(self) -> List[int]:
        return self._rsrc.query_binary_values('WAV:DATA?', datatype='s')
        #return self._rsrc.query('WAV:DATA?')

    def waveform_source(self, source:WaveformSource, index:int=1):
        self._rsrc.write(f'WAV:SOUR {source}{index}')

    def system_setup_save(self, filepath:str) -> None:
        tmp = self._rsrc.query_binary_values('SYST:SET?', datatype='B', container=bytearray)

        with open(filepath, 'wb') as f:
            f.write(tmp)

    def system_setup_recall(self, filepath:str) -> None:
        with open(filepath, 'rb') as f:
            tmp = f.read()
        self._rsrc.write_binary_values('SYST:SET ', tmp, datatype='B')

    def display_data(self) -> bytearray:
        self._rsrc.write('HARD:INKS OFF')
        return self._rsrc.query_binary_values('DISP:DATA? PNG, COL', 'B', container=bytearray)