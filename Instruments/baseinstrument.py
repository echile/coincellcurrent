import pyvisa as visa
from typing import NamedTuple

class InstrumentSystemError(NamedTuple):
    code: int
    msg: str

class StandardEventStatus(NamedTuple):
    pon:bool
    urq:bool
    cme:bool
    exe:bool
    dde:bool
    qye:bool
    rql:bool
    opc:bool
    reg:int

class BaseInstrument():
    rsrc: visa.resources.Resource
    def __init__(self, addr:str,timeout=2000):
        rm = visa.ResourceManager()
        self._rsrc = rm.open_resource(addr)   
        self._rsrc.timeout = timeout

    def iden(self):
        return self._rsrc.query('*IDN?')
    
    def reset(self):
        self._rsrc.write('*RST')
    
    def clear(self):
        self._rsrc.clear()
    
    def clear_status(self):
        self._rsrc.write('*CLS')

    def opc(self):
        self._rsrc.write('*WAI')

    def opc(self):
        self._rsrc.write('*OPC')

    def standard_event_status_enable(self):
        self._rsrc.write('*ESE')

    def standard_event_status_register(self):
        esr = int(self._rsrc.query('*ESR?').strip())
        return StandardEventStatus(pqn=(esr >> 7) & 1,
            urq=(esr >> 6) & 1, cme=(esr >> 5) & 1,
            exe=(esr >> 4) & 1, dde=(esr >> 3) & 1,
            qye=(esr >> 2) & 1, rql=(esr >> 1) & 1,
            opc=esr & 1, reg = esr)



    
    def opc_query(self):
        return  '1' in self._rsrc.query('*OPC?')

    def system_error(self):
        resp = self._rsrc.query('SYST:ERR?')
        code, msg = resp.split(',', maxsplit=1)

        return InstrumentSystemError(int(code), msg.strip())
        
